import './App.css'
import {
  Link
} from "react-router-dom"
import { useSelector, useDispatch } from 'react-redux'
import { useState } from 'react';
import { setTypeValue } from './store/reduxslice';

function Home() {
  const dispatch = useDispatch();
  const [type, settype]= useState('');
  const handleChange = (type1)=> (event) => {
   dispatch(setTypeValue(type1));
  }
  return (
    <div className="Home">
     <center>SELECT ANY OPTION </center>

   <div className="full">

      <div className="choose">
     <Link to="/entry" state={{ from: "Sanitary Pads" }}><button className="btn" id="1" onClick={handleChange("Pads")}>Sanitary Pads </button> </Link>
    <Link to="/entry" state={{ from: "Masks" }}><button className="btn" id="2" onClick={handleChange("Mask")}>Masks</button> </Link>
    </div>

    <div className="choose">
    <Link to="/entry" state={{ from: "Mensuration Caps" }}> <button className="btn" id="1"  onClick={handleChange("Caps")}>Mensuration Caps</button> </Link>
    <Link to="/entry" state={{ from: "Condoms" }}> <button className="btn" id="2" onClick={handleChange("")}>Condoms</button> </Link>
     </div>
     </div>
    </div>
  );
}

export default Home;