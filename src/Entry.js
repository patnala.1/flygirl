import './App.css';
import { Link, useLocation } from "react-router-dom"
import { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { setNumberValue } from './store/reduxslice';
 
function Entry() {
  const type = useSelector((state) => state.type.value);
  const dispatch = useDispatch();
  const location = useLocation()
  const [Number, setNumber] = useState(0);
  const handleChange = (e) => {
 dispatch(setNumberValue(e.target.value));
 setNumber(e.target.value);
  }
  const {from}  = location.state
  return (
    <div className="Entry">
    
    <center> Enter No of {type} : {from}</center>
         <input type="text" className="text2" value={Number} onChange={handleChange}></input>
         {
          !Number ?  <button className="test" disabled> submit</button>
          :  <Link to='/size' state={{Num :{ Number}}}> <button className="test"> submit</button> </Link> 
         }
        
    </div>
  );
}

export default Entry;