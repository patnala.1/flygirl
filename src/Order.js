import './App.css' ;
import { Link, useLocation } from "react-router-dom"
import Pet from './Images/Pet.png'
import { useSelector } from 'react-redux';
function Order () {
    const type = useSelector((state) => state.type.value);
    const size = useSelector((state) => state.size.value);
    const Number = useSelector((state) => state.Number.value);
    return(

<div className="Order">
   
   <div img="order"></div>
  <center className="credt">
    <h3 className='line'>{type} - {size}</h3> 
    <h2>Grand Total {Number}</h2> 

    <Link to='/payment'><button className='ord'> proceed for payment</button> </Link>
    </center>
</div>


    );
}

export default Order;