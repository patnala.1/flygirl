import './App.css';
import { useSelector, useDispatch } from 'react-redux'
import { useState } from 'react';
import { setSizeValue } from './store/reduxslice';
import { Link } from 'react-router-dom';
function Size() {
  const dispatch = useDispatch();
  const [size, settype]= useState('');
  const handleChange = (type1)=> (event) => {
   dispatch(setSizeValue(type1));
  }
  return (
    <div className="Home">
     <center>SELECT YOUR SIZE </center>

   <div className="full">

      <div className="choose">
     <Link to='/order' ><button  className="btn" id="1" onClick={handleChange("Regular")}>Regular</button> </Link>
     <Link to='/order'>  <button className="btn" id="2" onClick={handleChange("Large")}>Large</button></Link>
    </div>

    <div className="choose">
    <Link to='/order'>  <button className="btn" id="1" onClick={handleChange("Extra-Large")}>Extra-large</button></Link>
    <Link to='/order'>  <button className="btn" id="2" onClick={handleChange("XXL")}>XXL</button></Link>
     </div>
     </div>
    </div>
  );
}

export default Size;