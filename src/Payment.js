import './App.css';
import payment from './Images/Payment.png';
import { useState, useEffect } from 'react';
import { useLocation, Navigate } from "react-router-dom";


 
function Payment() {
  const [ minutes, setMinutes ] = useState(0);
    const [seconds, setSeconds ] =  useState(5);
    useEffect(()=>{
    let myInterval = setInterval(() => {
            if (seconds > 0) {
                setSeconds(seconds - 1);
            }
            if (seconds === 0) {
                if (minutes === 0) {
                    clearInterval(myInterval)
                } else {
                    setMinutes(minutes - 1);
                    setSeconds(59);
                }
            } 
        }, 1000)
        return ()=> {
            clearInterval(myInterval);
          };
    });
  return (
    <div className="Payment">
       { minutes === 0 && seconds === 0
            ? <Navigate to="/success" replace />
            : <center> <h1> {minutes}:{seconds < 10 ?  `0${seconds}` : seconds}</h1> 
            <h1 className='pr'> <center> SCAN THE QR CODE</center> </h1>
            <img src={payment} className="pic"/>   
            </center>
        }
        
    </div>
  );
}

export default Payment;
