import { configureStore } from '@reduxjs/toolkit'
import { TypeReducer,SizeReducer, NumberReducer } from './reduxslice'

export default configureStore({
    reducer: {
     type : TypeReducer,
     Number : NumberReducer,
     size : SizeReducer
    },
  })