import { createSlice } from '@reduxjs/toolkit'

export const TypeSlice = createSlice({
    name: 'type',
    initialState: {
      value: '',
    },
    reducers: {
      setTypeValue: (state, action) => {
        state.value = action.payload
      },
    },
})

export const NumberSlice = createSlice({
    name: 'Number',
    initialState: {
      value: 0,
    },
    reducers: {
      setNumberValue: (state, action) => {
        state.value = action.payload
      },
    },
})

export const SizeSlice = createSlice({
    name: 'size',
    initialState: {
      value: '',
    },
    reducers: {
      setSizeValue: (state, action) => {
        state.value = action.payload
      },
    },
})


export const {setNumberValue} = NumberSlice.actions
export const {setTypeValue} = TypeSlice.actions
export const {setSizeValue} = SizeSlice.actions

export const TypeReducer = TypeSlice.reducer
export const SizeReducer = SizeSlice.reducer
export const NumberReducer = NumberSlice.reducer