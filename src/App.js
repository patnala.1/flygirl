import './App.css';
import car from './Images/Home1.png';
import Home from './Home'
import Entry from './Entry.js'
import Loading from './Loading';
import Payment from './Payment';
import Sucessfull from './Sucessfull';
import Invalid from './Invalid';
import Identity from './Identity';
import Order from './Order';
import Size from './Size';

import {
  BrowserRouter as Router,
  Routes,
  Route,
  Link
} from "react-router-dom";
import Home1 from './Home1';
import store from './store/store';
import { Provider } from 'react-redux';

 
function App() {
  return (
    <Provider store={store}>
    <Router>
    <div>
    <Routes>
    <Route exact path="/" element={<Home1/>} />
    <Route path="/home" element={<Home/>} />
    <Route path="/entry" element={<Entry/>} />
    <Route path="/loading" element={<Loading/>} />
    <Route path="/order" element={<Order/>} />
    <Route path="/size" element={<Size/>} />
    <Route path="/invalid" element={<Invalid/>}/>
    <Route path="/identity" element={<Identity/>} />
    <Route path="/payment" element={<Payment/>} />
    <Route path="/success" element={<Sucessfull/>} />
    </Routes>
    </div>
  </Router>
</Provider>
  );
}

export default App;
